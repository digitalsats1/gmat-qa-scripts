# Quality Assurance GMAT Script Test Tools

Run GMAT script text suite in batch comparing with known results.[^1]

### Steps to run the quality assurance (QA) tests on GMAT scripts.

#### Prerequisites
- GMAT Ubuntu 20.04 OVA installed, running and being used for the test.
  [http://download.digitalsats.com](OVA/Vagrant large download)
- Account on GitLab established.
  [https://gitlab.com](Join GitLab)
- *(already in OVA)* Git utility installed.[^2]
  `sudo apt install git`
- *(already in OVA)* Python3 language and utilities installed.[^2]
  `sudo apt install python3`
- GMAT configuration setup.
  *Your setup may differ.*
  - Edit /home/vagrant/gmat/application/bin/gmat_startup_file.txt
    - If you don't have MATLAB or OCTAVE, then comment out the
      MATLAB related lines (comment lines start with #):
      ```
      #PLUGIN   = ../plugins/libMatlabInterface
      #PLUGIN   = ../plugins/libFminconOptimizer
      ...
      #MATLAB_FUNCTIOL_PATH   = ROOT_PATH/matlab
      #MATLAB_FUNCTIOL_PATH   = ROOT_PATH/userfunctions/matlab
      ```
    - Save the startup file.
- *(optional) Add yourself to user vagrant's group.[^3]
  - Since GMAT will use the directories containing the GMAT files by
    default, you will need to do one of the two options:
    1. be logged in as user **vagrant** when running GMAT
    2. add your userid to the **vagrant** group
  - If you will be using the standard OVA for development, and will be
    logging in as yourself (option 2 above), then you can save
    yourself some grief by adding yourself to the **vagrant** group.
    For example, if you will be using user ***myname***, then do
    ```
    sudo usermod -g myname -G myname,vagrant myname
    ```
    You can examine your work via
    ```
    id myname
    ```
    and you should see that you are a member of two groups,
    ***myname*** and ***vagrant***.
    ```
    id myname
    uid=1002(myname) gid=1002(myname) groups=1002(myname),1000(vagrant)
    ```


#### Run the QA Suite

1. Create a test directory.
```bash
mkdir smallsats
cd smallsats
```

2. Download the test GMAT scripts.
```bash
git clone https://gitlab.com/digitalsats/gmat-collected-scripts.git
```
This creates the directory `smallsats/gmat-collected-scripts`

3. Download the QA Python scripts.
```bash
git clone https://gitlab.com/digitalsats/gmat-qa-scripts.git
```
This creates the directory `smallsats/gmat-qa-scripts`

4. Run the QA test of GMAT scripts.
```bash
gmat-qa-scripts/rungmat gmat-collected-scripts/README.md
```
The QA suite will take just under an hour (depending on your machine) and you will notice that the GMAT IDE will completely restart for each GMAT script. The restarts are important so that each GMAT script's run data does not pollute the others.

- Please take a look at the QA_TestScripts.py source code for more information.
- You are encourage to reach out to the developers with constructive comments on how to improve the tests. Communicate with @jkkastner


Footnotes:

[^1]: This code is still under active development and changes frequently.
[^2]: This step should not be necessary as the OVA already contains
      the code. However the information is included here for completeness.
[^3]: An example of the errors you might expect if logged in as yourself,
      using the vagrant user's files, and not being in the vagrant
      group is:
      ```
      can't open user configuration file
      Failed to create a temporary file name (error 13: Permission denied)
      can't open user configuration file
      ```
      While these errors are not fatal and you can proceed past them,
      they are very annoying.
